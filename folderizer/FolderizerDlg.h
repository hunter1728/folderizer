// FolderizerDlg.h : header file
//

#pragma once

#include <vector>
#include <set>


class CImageFile
{
public:
	CString sName;
	CString sDateTaken;
	CString sTimeTaken;
};

// CFolderizerDlg dialog
class CFolderizerDlg : public CDialogEx
{
// Construction
public:
	CFolderizerDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_FOLDERIZER_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support


// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedPickfolder();
	std::vector<CImageFile> GetFilesInDirectory(const CString &sDirectory);
	CString Folderize(const std::vector <CImageFile> & Files, const CString & sFolderName);
	std::set <CString> GetUniqueFileDates(const std::vector <CImageFile> & Files);
	void CreateDateDirectories(const std::vector <CImageFile> & Files, const CString & sFolderName);
	CString MoveFiles(const std::vector <CImageFile> & Files, const CString & sFolderName);
	CString RenameImgFiles(const std::vector <CImageFile>& Files, const CString& sFolderName);
	CString GetFileDateString(const CString & sFile);
	CString GetFileTimeString(const CString& sFile);
	CString GetFileExifDateTime(const CString & sFile);
	CString m_sMessage;
	afx_msg void OnBnClickedPickfolderforrenames();
};
