// FolderizerDlg.cpp : implementation file
//

#include "stdafx.h"
#include "folderizer.h"
#include "FolderizerDlg.h"
#include "afxdialogex.h"
#include "easyexif-master\exif.h"

#include <set>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

using std::vector;
using std::set;

// CFolderizerDlg dialog

CFolderizerDlg::CFolderizerDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(IDD_FOLDERIZER_DIALOG, pParent)
	, m_sMessage(_T(""))
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CFolderizerDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_MSG, m_sMessage);
}

BEGIN_MESSAGE_MAP(CFolderizerDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_PICKFOLDER, &CFolderizerDlg::OnBnClickedPickfolder)
	ON_BN_CLICKED(IDC_PICKFOLDERFORRENAMES, &CFolderizerDlg::OnBnClickedPickfolderforrenames)
END_MESSAGE_MAP()


// CFolderizerDlg message handlers

BOOL CFolderizerDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	// TODO: Add extra initialization here

	return TRUE;  // return TRUE  unless you set the focus to a control
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CFolderizerDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CFolderizerDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

/**********************************************
**********************************************/
void CFolderizerDlg::OnBnClickedPickfolder()
{
	CFolderPickerDialog m_dlg;

	m_dlg.m_ofn.lpstrTitle = _T("Put your title here");
	m_dlg.m_ofn.lpstrInitialDir = _T("C:\\");
	if (m_dlg.DoModal() != IDOK)
	{
		return;
	}

	CString sFolderName = m_dlg.GetPathName();
	sFolderName += _T("\\");

	auto Files = GetFilesInDirectory(sFolderName);


	auto sLog = Folderize(Files, sFolderName);

	sLog += "Done!";
	m_sMessage = sLog;
	UpdateData(FALSE);
}


/**********************************************
**********************************************/
void CFolderizerDlg::OnBnClickedPickfolderforrenames()
{
	CFolderPickerDialog m_dlg;

	m_dlg.m_ofn.lpstrTitle = _T("Put your title here");
	m_dlg.m_ofn.lpstrInitialDir = _T("C:\\");
	if (m_dlg.DoModal() != IDOK)
	{
		return;
	}

	CString sFolderName = m_dlg.GetPathName();
	sFolderName += _T("\\");

	auto Files = GetFilesInDirectory(sFolderName);


	auto sLog = RenameImgFiles(Files, sFolderName);

	sLog += "Done!";
	m_sMessage = sLog;
	UpdateData(FALSE);
}


/**********************************************
 From stackoverflow
**********************************************/
std::vector<CImageFile> CFolderizerDlg::GetFilesInDirectory(const CString &sDirectory)
{
	std::vector<CImageFile> out;

	HANDLE dir;
	WIN32_FIND_DATA file_data;

	if ((dir = FindFirstFile((sDirectory + "/*"), &file_data)) == INVALID_HANDLE_VALUE)
		return out; /* No files found */

	do {
		const CString file_name = file_data.cFileName;
		const CString full_file_name = sDirectory + file_name;
		const bool is_directory = (file_data.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) != 0;

		if (file_name[0] == '.')
			continue;

		if (is_directory)
			continue;

		CImageFile Image;
		Image.sName = full_file_name;

		// Get the image date here
		Image.sDateTaken = GetFileDateString(Image.sName);
		Image.sTimeTaken = GetFileTimeString(Image.sName);


		out.push_back(Image);


	} while (FindNextFile(dir, &file_data));

	FindClose(dir);

	return out;
} // GetFilesInDirectory


/**********************************************
**********************************************/
CString CFolderizerDlg::Folderize(const vector <CImageFile> & Files, const CString & sFolderName)
{
	CreateDateDirectories(Files, sFolderName);

	// return the log
	return MoveFiles(Files, sFolderName);
}

/**********************************************
**********************************************/
void CFolderizerDlg::CreateDateDirectories(const vector <CImageFile> & Files, const CString & sFolderName)
{
	auto Dates = GetUniqueFileDates(Files);

	for (auto Date : Dates)
	{
		_wmkdir(sFolderName + Date);
	}
}

/**********************************************
**********************************************/
CString CFolderizerDlg::MoveFiles(const vector <CImageFile> & Files, const CString & sFolderName)
{
	CString sLog;
	for (auto File : Files)
	{
		CString sFileName = File.sName.Mid(File.sName.ReverseFind('\\') + 1);
		CString sDestPath = sFolderName + File.sDateTaken + "\\" + sFileName;

		// From the docs:  The new name must not already exist.
		auto ReturnVal = MoveFile(File.sName, sDestPath);
		if (ReturnVal == 0)
		{
			auto LastError = GetLastError();
			if (LastError == 183)
			{
				// The move failed, because the file already existed.

				// Now what? 
				// Auto-overwrite seems dangerous - what if it's not the same file?
				// Move with new name seems complicated
				// Right now we leave them behind- but I'm afraid there's some other scenario and I end up losing a picture?

				// We don't really care about performance, so for now, just note it in a log
				CString sMsg;
				sMsg.Format(_T("File %s already exists in %s\n"), sFileName, sDestPath);
				sLog += sMsg;
			}
		}
	}

	return sLog;
}


/**********************************************
**********************************************/
CString CFolderizerDlg::RenameImgFiles(const vector <CImageFile>& Files, const CString& sFolderName)
{
	CString sLog;
	for (auto File : Files)
	{
		CString sFileName = File.sName.Mid(File.sName.ReverseFind('\\') + 1);

		// Make sure the file name doesn't start with a number already (to avoid doubling things up)
		if (!isdigit(sFileName[0]))
		{
			CString sCleanTime = File.sTimeTaken;
			sCleanTime.Remove(':');
			CString sDestPath = sFolderName + "\\" + File.sDateTaken + " " + sCleanTime + " " + sFileName;

			// From the docs:  The new name must not already exist.
			auto ReturnVal = MoveFile(File.sName, sDestPath);
			if (ReturnVal == 0)
			{
				auto LastError = GetLastError();
				if (LastError == 183)
				{
					// The move failed, because the file already existed.

					// Now what? 
					// Auto-overwrite seems dangerous - what if it's not the same file?
					// Move with new name seems complicated
					// Right now we leave them behind- but I'm afraid there's some other scenario and I end up losing a picture?

					// We don't really care about performance, so for now, just note it in a log
					CString sMsg;
					sMsg.Format(_T("File %s already exists in %s\n"), sFileName, sDestPath);
					sLog += sMsg;
				}
			}
		}
	}

	return sLog;
}

/**********************************************
**********************************************/
CString CFolderizerDlg::GetFileDateString(const CString & sFile)
{
	CString sExifDateTimeString = GetFileExifDateTime(sFile);
	if (sExifDateTimeString.GetLength() > 0)
	{
		// cut off the time
		CString sDate = sExifDateTimeString.Left(sExifDateTimeString.Find(_T(" ")));
		sDate.Replace(_T(":"), _T("-"));

		return sDate;
	}

	CFileStatus fs;
	CFile::GetStatus(sFile, fs);
	return fs.m_mtime.Format("%Y-%m-%d");
}


/**********************************************
**********************************************/
CString CFolderizerDlg::GetFileTimeString(const CString& sFile)
{
	CString sExifDateTimeString = GetFileExifDateTime(sFile);
	if (sExifDateTimeString.GetLength() > 0)
	{
		// Grab the last 8 positions
		CString sTime = sExifDateTimeString.Right(8);

		return sTime;
	}

	CFileStatus fs;
	CFile::GetStatus(sFile, fs);
	return fs.m_mtime.Format("%T");
}

/**********************************************
**********************************************/
CString CFolderizerDlg::GetFileExifDateTime(const CString & File)
{
	// From exif code demo code:

	// Read the JPEG file into a buffer
	FILE *fp;
	_wfopen_s(&fp, File, _T("rb"));
	if (!fp) {
		printf("Can't open file.\n");
		return _T("");
	}
	fseek(fp, 0, SEEK_END);
	unsigned long fsize = ftell(fp);
	rewind(fp);
	unsigned char *buf = new unsigned char[fsize];
	if (fread(buf, 1, fsize, fp) != fsize) {
		printf("Can't read file.\n");
		delete[] buf;
		return _T("");
	}
	fclose(fp);

	// Parse EXIF
	easyexif::EXIFInfo result;
	int code = result.parseFrom(buf, fsize);
	delete[] buf;
	if (code) {
		printf("Error parsing EXIF: code %d\n", code);
		return _T("");
	}

	return CString(result.DateTime.c_str());
}

/**********************************************
**********************************************/
set <CString> CFolderizerDlg::GetUniqueFileDates(const vector <CImageFile> & Files)
{
	// use set to consolidate duplicates
	set <CString> Dates;

	for (auto File : Files)
	{
		Dates.insert(File.sDateTaken);
	}

	return Dates;
}

